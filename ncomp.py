import collections


# TODO: For testing take a n-dim board, and generate all cubical sets
#       by removing edges. Then try copying these, and test the equality
#       of the copies.


def face_map(sdim, pos, _cache={}):
    r = _cache.get((sdim, pos), None)
    if r is not None:
        return r

    # sdim is the sdim of the composed cells.
    # pos is the dim of the source of the composition.
    # Returns the sdim of the edges that are equated
    # in the composition.
    r = list(range(sdim))
    for i in range(pos, sdim):
        r[i] += 1

    r = tuple(r)
    _cache[(sdim, pos)] = r
    return r


class Error(Exception):
    def __init__(self, node1, node2):
        self.node1 = node1
        self.node2 = node2
        super().__init__(self.message)


class ExpansionError(Error):
    message = "expansion error"


class CompositionError(Error):
    message = "composition error"


class Edge:
    def __init__(self, value, source, target, sdim):
        self.value = value
        self.source = source
        self.target = target
        self.sdim = sdim

    @property
    def label(self):
        return self.value

    def __repr__(self):
        return ('Edge('
                + self.source.label
                + '-' + (self.label or '=')
                + '->' + '>'*self.sdim
                + self.target.label
                + ')')

    def node(self):
        print('node(')
        sdim = self.sdim
        # These copies should not pass beyond nodes of too high a dim.
        # When copying the target, such is indicated by the optarget.
        # Copying of the source should determine the target of the target,
        # so that the target is copied correctly, that is, with the correct
        # target of the target.
        stop_set = set()
        s = self.source.copy(sdim, source_stop_set=stop_set)
        t = self.target.copy(sdim, target_stop_set=stop_set)
        s.expand(self.value, t)
        print(')node')
        return s


class Node:
    def __init__(self, value):
        self.value = value
        self.opsource = dict()

    def __repr__(self):
        return 'Node(' + self.label + ')'

    @property
    def label(self):
        return self.value

    def node(self):
        return Node(self.value)

    def edge(self, dim):
        if dim == 0:
            return self
        return self.opsource[dim-1]

    @property
    def dim(self):
        # The skipped sdim is the dimension
        for sdim in range(len(self.opsource) + 1):
            if sdim not in self.opsource:
                return sdim

    def eq(self, dim, node):
        #print('eq', self, node)
        if dim < 0:
            return True

        if self.value != node.value:
            return False

        for d in range(dim):
            if d in self.opsource:
                if d in node.opsource:
                    if self.opsource[d].value != node.opsource[d].value:
                        return False
                else:
                    return False
            elif d in node.opsource:
                return False

        return True

    def walk(self, dim):
        # dim is the dim of the node as a source for the cell corresponding
        # to the edge.
        node = self
        while dim in node.opsource:
            edge = node.opsource[dim]
            node = edge.target
            yield edge

    def tree(self, dims):
        dims = list(dims)
        if not dims:
            yield self, None
        else:
            dim = dims.pop()
            yield None, self.tree(dims)
            for edge in self.walk(dim):
                yield edge, edge.target.tree(dims)

    def copy(self, dim, source_stop_set=None, target_stop_set=None):
        return Composer(self.tree(range(dim)),
                        dim,
                        FreeMon,
                        source_stop_set,
                        target_stop_set).result()

    def source_tree(self, dim):
        if dim == self.dim:
            return self.tree(range(self.dim))
        return self.tree(face_map(self.dim-1, dim))

    def target_tree(self, dim):
        if dim == self.dim:
            return self.tree(range(self.dim))
        for edge in self.walk(dim):
            continue
        return edge.target.tree(face_map(self.dim-1, dim))

    def expand(self, value, target):
        dim = self.dim
        assert(dim == target.dim)

        if dim == 0:
            self.opsource[0] = Edge(value, self, target, dim)
            return

        sdim = dim - 1
        ss = self.source_tree(sdim)
        st = self.target_tree(sdim)
        ts = target.source_tree(sdim)
        tt = target.target_tree(sdim)
        self._bind_trees(ss, ts, dim, value)
        self._bind_trees(st, tt, dim, None)

    @classmethod
    def _bind_trees(cls, tree1, tree2, dim, value):
        it = zip(tree1, tree2)

        for (e1, b1), (e2, b2) in it:
            if e1 is None:
                cls._bind_trees(b1, b2, dim, value)
            else:
                if not e1.eq(dim-1, e2):
                    raise ExpansionError(e1, e2)

                e1.opsource[dim] = Edge(value, e1, e2, dim)
                return
            break

        for (e1, b1), (e2, b2) in it:
            cls._bind_trees(b1, b2, dim, None)

        for e1 in tree1:
            raise ExpansionError(e1, None)

        for e2 in tree2:
            raise ExpansionError(None, e2)

    def compose(self, dim, factor):
        assert(self.dim == factor.dim)
        print('compose', dim)
        target = self.target_tree(dim)
        source = factor.source_tree(dim)
        self._stick_trees(target, source, dim, self.dim)

    @classmethod
    def _stick_trees(cls, tree1, tree2, pos, dim):
        it = zip(tree1, tree2)

        for (e1, b1), (e2, b2) in it:
            if e1 is None:
                cls._stick_trees(b1, b2, pos, dim)
            else:
                if not e1.eq(pos, e2):
                    raise CompositionError(e1, e2)

                # Some edges are already equal,
                # namely the non-shifted ones.
                for d in range(pos+1, dim):
                    # d is not present in target nodes.
                    if d in e1.opsource:
                        e1.opsource[d].value = e2.opsource[d].value

                e1.opsource[pos] = e2.opsource[pos]

            break

        for (e1, b1), (e2, b2) in it:
            cls._stick_trees(b1, b2, pos, dim)

        for e1 in tree1:
            raise CompositionError(e1, None)

        for e2 in tree2:
            raise CompositionError(None, e2)

    def tree_repr(self, dim=None):
        if dim is None:
            dim = self.dim

        def write_node(n):
            result.append(n.label)

        def write_edge(e):
            sdim = e.sdim
            if sdim == 0:
                start = "-"
                end = "->"
            else:
                start = "\n|"*sdim + "\n"
                end = start + "V\n"

            result.append(start)
            result.append(e.label or '=')
            result.append(end)

        def foreach(tree):
            for e, b in tree:
                if e:
                    write_node(e)
                    return

                foreach(b)
                break

            for e, b in tree:
                write_edge(e)
                foreach(b)

        result = []
        foreach(self.tree(range(dim)))
        return ''.join(result)


class ComposerStack(collections.defaultdict):
    def __init__(self, mon):
        super().__init__()
        self.mon = mon

    def __missing__(self, key):
        r = self.mon(key)
        self[key] = r
        return r

    def init(self, e):
        self[0].unit(e)

    def reduce(self, key):
        print('reduce', self)
        c = self.pop(key, None)
        print('reduce', key, c)
        if c:
            self[key+1].mul(c.total)


class Composer:
    class TreeState:
        def __init__(self, sdim):
            self.sdim = sdim
            self.stop = False

    def __init__(self, tree, dim, mon, source_stop_set, target_stop_set):
        self.tree = tree
        self.is_start = True
        self.stack = ComposerStack(mon)
        self.dim = dim
        self.source_stop_set = source_stop_set
        self.target_stop_set = target_stop_set

    def _feed_node(self, node, state):
        if node.dim == self.dim or (node.dim > self.dim and self.is_start):
            if self.target_stop_set and node in self.target_stop_set:
                state.stop = True
            else:
                # If the edge starts a new cube then do not add it.
                self.stack.init(node.edge(self.dim))
                self.is_start = False
        elif self.target_stop_set:
            # Target copying must be parallel to source copying.
            #if self.dim >= 1: print('t', node, node.opsource, id(node))
            if node in self.target_stop_set:
                state.stop = True
        elif node.dim > self.dim or state.sdim not in node.opsource:
            # Indicate that the walk with dimension wdim has terminated.
            state.stop = True

            # Needed for correctly copying the target of an edge.
            if self.source_stop_set is not None:
                #if self.dim >= 1: print('s', node, node.opsource, id(node.opsource[self.dim].target))
                self.source_stop_set.add(node.opsource[self.dim].target)

    def _compose(self, tree, state, sdim):
        for e, branch in tree:
            #print('first', e, wdim, self.wdim)
            if branch is None:
                # e is a node
                # node is final only for one dimension (this is a tree)
                # It is final for the highest dimension that walks the node.
                self._feed_node(e, state)
                return

            # First node can indicate whether any of the previous walks ends.
            # The first branch cannot end the current walk. Each walk has
            # at least one edge.
            sdim -= 1
            self._compose(branch, state, sdim)
            break

        bstate = Composer.TreeState(sdim)
        for e, branch in tree:
            #print(e, wdim, self.wdim)
            print('e', e)
            self.stack.reduce(sdim)
            # First node can indicate only whether the current dim ends.
            # When decompose is finished all lower dim stops have been applied.
            self._compose(branch, bstate, sdim)
            if bstate.stop:
                break

    def result(self):
        self._compose(self.tree, Composer.TreeState(self.dim), self.dim)
        print('result', self.dim, self.stack)
        print('stop_set', self.source_stop_set, self.target_stop_set)
        return self.stack[self.dim].total


class ListLabelMon:
    def __init__(self, dim):
        self.total = []

    def unit(self, value):
        self.total = value.label

    def mul(self, value):
        self.total.append(value)


class FreeMon:
    def __init__(self, dim):
        self.total = None
        self.dim = dim

    def __repr__(self):
        return ('FreeMon('
                + repr(self.dim) + ', '
                + repr(self.total)
                + ')')

    def unit(self, value):
        print('unit', value)
        self.total = value.node()

    def mul(self, value):
        print('mul', value)
        if self.total is None:
            self.total = value
            return

        self.total.compose(self.dim-1, value)


class ApplyMon:
    def __init__(self, dim):
        self.total = None
        self.dim = dim

    def apply(self, dim, arg, func):
        raise NotImplementedError()

    def load(self, label):
        raise NotImplementedError()

    def unit(self, value):
        self.total = self.load(value.label)

    def mul(self, value):
        if self.total is None:
            self.total = value
            return

        # total has the term and value is the function
        self.total = self.apply(self.dim-1, self.total, value)


class CodeMon:
    def __init__(self, dim):
        self.total = []
        self.dim = dim
