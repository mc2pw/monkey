from . import ncomp


class Value:
    def __init__(self):
        self._label = None
        self._concrete = None
        self._opinv = None
        self._opid = None

    def __eq__(self, value):
        pass

    def apply(self, dim, value):
        return

    @property
    def label(self):
        if self._opinv:
            return "~" + self._opinv.label
        if self._opid:
            return self._opid.label
        return self._label

    @property
    def concrete(self):
        return self._concrete #...

    @staticmethod
    def abstract(label):
        r = Value()
        r._label = label
        return r

    def inverse(self):
        if self._opinv:
            return self._opinv

        r = Value()
        r._opinv = self
        return r

    def identity(self):
        r = Value()
        r._opid = self
        return r

    def copy_from(self, value):
        self._label = value._label
        self._concrete = value._concrete
        self._opinv = value._opinv
        self._opid = value._opid

class DiagramMeta(type):
    def __new__(cls, name, bases, attrs):
        # Introduce properties to allow implementation of interface
        return type.__new__(cls, name, bases, attrs)

    def __init__(self, name, bases, attrs):
        self._init_cells(attrs)

def diagram(dim_num):
    class Diagram(metaclass=DiagramMeta):
        dim = dim_num

        def __init__(self):
            # Diagram is initialized in order to implement it.
            # Implementing a cell means assigning it.
            # During assignment a copy of the cell is created
            # and stored in the instance. The value of this
            # copy is set to the the implemented value.
            # An implementation is a strict functor relation.
            # Implementing a cell requires implementing its
            # source and target.
            # Abstract execution for drawing diagrams, etc.
            # The __init__ of the subclass can provide
            # implementations of some cells.
            # This way an inherited builtin interface can be
            # implemented by calling super().__init__.
            # Alternative implementations of the builtin can
            # be obtained by initializing the builtin cells
            # with a different method.
            # Concrete cells don't have labels.
            # Getting a non implemented concrete cell raises
            # an error.
            pass

        @classmethod
        def _init_cells(cls, attrs):
            for key in attrs:
                cell = attrs[key]
                if not isinstance(cell, Cell):
                    continue

                cell.set_value(Value.abstract(key))
                if cls.dim <= cell.node.dim:
                    cell.set_sym()



class Cell:
    def __init__(self, node=None):
        self.node = node or ncomp.Node(Value())
        self.sym = None

    def property(self):
        def get(s):
            return self.get_concrete(s.concrete_map)

        def set(s, c):
            self.set_concrete(s.concrete_map, c)

        return property(get, set)

    def ref(self, offset):
        if offset == 0:
            return self.node

        n = self.node
        dim = n.dim
        source = n.copy(dim)
        target = n.copy(dim)
        source.expand(n.value.identity(), target)
        return Cell(source).ref(offset-1)

    def same_dim_nodes(self, cell):
        offset = cell.node.dim - self.node.dim
        if offset < 0:
            n2, n1 = cell.same_dim_nodes(self)
            return n1, n2
        return self.ref(offset), cell.node

    def set_value(self, value):
        node = self.node
        node.edge(node.dim).value.copy_from(value)

    def set_sym(self):
        source = n2.copy(dim)
        target = n1.copy(dim)
        source.expand(value.inverse(), target)
        self.sym = Cell(source)
        self.sym.sym = self

    # Inverses, identities, adjunctions, etc. are handled at this level
    # not at the ncomp level.

    def __ge__(self, cell):
        n1, n2 = self.same_dim_nodes(cell)
        dim = n1.dim
        source = n1.copy(dim)
        target = n2.copy(dim)
        value = Value()
        source.expand(value, target)
        return Cell(source)

    def comp(self, dim, cell):
        # Handle composition with the identity?
        # No. Composition with the identity is handled
        # as the execution.

        n1, n2 = self.same_dim_nodes(cell)
        dim = n1.dim
        n1 = n1.copy(dim)
        n2 = n2.copy(dim)
        n1.compose(dim, n2) #dim?
        return Cell(n1)

    def __xor__(self, cell):
        return self.comp(2, cell)

    def __and__(self, cell):
        return self.comp(1, cell)

    def __or__(self, cell):
        return self.comp(0, cell)

    def __invert__(self):
        return self.sym

