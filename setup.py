from setuptools import setup

setup(name='drako',
      version='0.1',
      description='Executable string diagrams in Python',
      url='https://gitlab.com/mc2pw/drako',
      author='Gabriel Peña',
      author_email='gabriel@mc2.pw',
      license='MIT',
      packages=['drako'],
      zip_safe=False)
